package de.andrena.ausbildung.einwohnerverwaltung;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;

import de.andrena.ausbildung.einwohnerverwaltung.beans.Address;
import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.beans.Person;
import de.andrena.ausbildung.einwohnerverwaltung.services.CitizenAdministration;
import de.andrena.ausbildung.einwohnerverwaltung.services.CitizenAdministrationImpl;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityService;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityServiceImpl;

public class Runner {

	private static final Logger logger = LogManager.getLogger(Runner.class);

	public static void main(String[] args) {

		logger.info("Starte Einwohnerverwaltung");

		CityService cityService = CityServiceImpl.instance();
		CitizenAdministration citizAdmin = CitizenAdministrationImpl.instance();

		City city = cityService.createCity("Karl");
		cityService.createCity("Berlin");
		cityService.createCity("Ettlingen");
		cityService.createCity("Muenchen");

		Person person = citizAdmin.createCitizen("Meier", "Peter", new LocalDate(1994, 2, 3));
		Address address = new Address(city, "22334", "Dingsstraße", 4);
		citizAdmin.changeRegistration(person, address);

		cityService.exportCities();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		cityService.importCities();
		List<City> cities = cityService.getAllCities();
		logger.info(person);
		logger.info(cities.get(0).containsCitizen(person) ? "Ja" : "Nein");

		logger.info(cities);

		logger.info("Einwohnerverwaltung beendet");
	}

}
