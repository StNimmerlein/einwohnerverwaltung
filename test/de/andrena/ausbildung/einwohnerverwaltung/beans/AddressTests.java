package de.andrena.ausbildung.einwohnerverwaltung.beans;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import de.andrena.ausbildung.einwohnerverwaltung.beans.Address;
import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityService;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityServiceImpl;

/**
 * 
 * @author aosintseva
 *
 */
@RunWith(Parameterized.class)
public class AddressTests {

	public static CityService cityService;

	@Parameter(0)
	public Address address1;

	@Parameter(1)
	public Address address2;

	@Parameter(2)
	public Boolean equalsResult;

	public static void initCityService() {
		cityService = CityServiceImpl.instance();
	}

	@Parameters
	public static List<Object[]> initParameters() {
		initCityService();
		City city1 = createCity("Karlsruhe");
		City city2 = createCity("Berlin");
		return Arrays.asList(new Object[][] { { new Address(city1, "76131", "Albert-Nestler-Stra�e", 9, "a"), new Address(city1, "76131", "Albert-Nestler-Stra�e", 9, "a"), true },
				{ new Address(city1, "76131", "Albert-Nestler-Stra�e", 9, "a"), new Address(city2, "76131", "Albert-Nestler-Stra�e", 9, "a"), false },
				{ new Address(city1, "76131", "Albert-Nestler-Stra�e", 9, "b"), new Address(city1, "76131", "Albert-Nestler-Stra�e", 9, "a"), false },
				{ new Address(city1, "76131", "Albert-Nestler-Stra�e", 19, "b"), new Address(city2, "76131", "Albert-Nestler-Stra�e", 9, "a"), false },
				{ new Address(city1, "76131", "Albert-Nestler-Stra�e", 9, "a"), new Address(city1, "76131", "Albert-Nestler-Stra�e", 9, "a"), true } });
	}

	@Test
	public void testConstructor() {
		Address address = new Address(createCity("Karlsruhe"), "76131", "Albert-Nestler-Stra�e", 9);
		assertTrue(address.getHouseNumber() == 9);
		assertTrue(address.getZipCode().equalsIgnoreCase("76131"));
		assertTrue(address.getStreetName().equalsIgnoreCase("Albert-Nestler-Stra�e"));
		assertTrue(address.getHouseNumberSupplement().equalsIgnoreCase(""));
	}

	@Test
	public void testFullConstructor() {
		Address address = new Address(createCity("Ettlingen"), "76131", "Albert-Nestler-Stra�e", 9, "a");
		assertTrue(address.getHouseNumber() == 9);
		assertTrue(address.getZipCode().equalsIgnoreCase("76131"));
		assertTrue(address.getStreetName().equalsIgnoreCase("Albert-Nestler-Stra�e"));
		assertTrue(address.getHouseNumberSupplement().equalsIgnoreCase("a"));
	}

	@Test
	public void testEquals() {
		Assert.assertEquals(equalsResult, address1.equals(address2));
	}

	private static City createCity(String name) {
		return cityService.createCity(name);
	}
}
