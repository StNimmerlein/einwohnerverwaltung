package de.andrena.ausbildung.einwohnerverwaltung.persistance;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.persistance.PersistenceFacadeFile;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityService;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityServiceImpl;

/**
 * 
 * @author aosintseva
 *
 */
public class PersistenceFacadeFileTest {

	public static String[] names = new String[] { "Karlsruhe", "Berlin", "Ettlingen", "Muenchen" };

	public static List<City> cities;

	public static PersistenceFacadeFile persistenceFacade;

	@BeforeClass
	public static void initCityService() {

		persistenceFacade = new PersistenceFacadeFile();

		CityService cityService = CityServiceImpl.instance();

		cities = new ArrayList<>();
		for (String name : names) {
			cities.add(cityService.createCity(name));
		}
	}

	@After
	public void deleteFile() {
		File file = new File(PersistenceFacadeFile.CITIES_FILE);
		file.delete();
	}

	@Test
	public void testSaveCity() {
		persistenceFacade.save(cities);
		File file = new File(PersistenceFacadeFile.CITIES_FILE);
		Assert.assertTrue(file.exists());

		List<City> savedCities = persistenceFacade.loadCities();
		Assert.assertEquals(cities, savedCities);
	}

}
