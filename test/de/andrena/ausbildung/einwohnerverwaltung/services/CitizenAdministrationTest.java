package de.andrena.ausbildung.einwohnerverwaltung.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import de.andrena.ausbildung.einwohnerverwaltung.beans.Address;
import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.beans.Person;
import de.andrena.ausbildung.einwohnerverwaltung.services.CitizenAdministration;
import de.andrena.ausbildung.einwohnerverwaltung.services.CitizenAdministrationImpl;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityService;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityServiceImpl;

public class CitizenAdministrationTest {

	private CityService cityService = CityServiceImpl.instance();
	private CitizenAdministration cityAdmin = CitizenAdministrationImpl.instance();
	private Person person;
	private City city;
	private Address address;

	@Before
	public void setUp() {
		person = cityAdmin.createCitizen("Musterfrau", "Petra", new LocalDate(1970, 01, 03));
		city = cityService.createCity("Hamburg");
		address = new Address(city, "01234", "Peterstraße", 7);
	}

	@Test
	public void testCreatePerson() {
		assertTrue(person.getLastName() == "Musterfrau");
		assertTrue(person.getFirstName() == "Petra");
		assertTrue(person.getDateOfBirth().equals(new LocalDate(1970, 01, 03)));
	}

	@Test
	public void testIsCitizen() {
		cityAdmin.changeRegistration(person, address);
		assertTrue(city.containsCitizen(person));
	}

	@Test
	public void testChangeRegistration() {
		City city2 = cityService.createCity("Buxtehude");
		Address address2 = new Address(city2, "11223", "Schnurzstraße", 4, "b");

		cityAdmin.changeRegistration(person, address);
		assertTrue(city.containsCitizen(person));

		cityAdmin.changeRegistration(person, address2);
		assertFalse(city.containsCitizen(person));
		assertTrue(city2.containsCitizen(person));
	}
}
