package de.andrena.ausbildung.einwohnerverwaltung.utils;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class IdGeneratorTest {

	@Test
	public void testGetNextId() {
		long id1 = IdGenerator.getNextId();
		long id2 = IdGenerator.getNextId();
		assertFalse(id1 == id2);
	}
}
