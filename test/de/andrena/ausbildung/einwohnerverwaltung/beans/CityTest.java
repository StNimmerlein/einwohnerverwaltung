package de.andrena.ausbildung.einwohnerverwaltung.beans;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.beans.Person;
import de.andrena.ausbildung.einwohnerverwaltung.services.CitizenAdministration;
import de.andrena.ausbildung.einwohnerverwaltung.services.CitizenAdministrationImpl;

public class CityTest {

	private City city;
	private CitizenAdministration cityAdmin = CitizenAdministrationImpl.instance();

	@Before
	public void initialize() {
		city = new City(1, "Karlsruhe");
	}

	@Test
	public void testCreateCity() {
		assertTrue(city.getName().equalsIgnoreCase("Karlsruhe"));
	}

	@Test
	public void testEquals() {
		City city2 = new City(2, "Karlsruhe");
		assertFalse(city.equals(city2));
	}

	@Test
	public void testAddCitizen() {
		Person person = cityAdmin.createCitizen("Schweinebär", "Mann", new LocalDate(1934, 3, 2));
		city.addCitizen(person);
		assertTrue(city.containsCitizen(person));
	}

	@Test
	public void testRemoveCitizen() {
		Person person = cityAdmin.createCitizen("Schweinebär", "Mann", new LocalDate(1934, 3, 2));
		city.addCitizen(person);
		assertTrue(city.containsCitizen(person));
		city.removeCitizen(person);
		assertFalse(city.containsCitizen(person));
	}

}
