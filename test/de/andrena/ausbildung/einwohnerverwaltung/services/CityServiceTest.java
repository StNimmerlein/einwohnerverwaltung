package de.andrena.ausbildung.einwohnerverwaltung.services;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.andrena.ausbildung.einwohnerverwaltung.beans.City;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityService;
import de.andrena.ausbildung.einwohnerverwaltung.services.CityServiceImpl;

public class CityServiceTest {

	private CityService cityService = CityServiceImpl.instance();
	private City city;

	@Before
	public void initialize() {
		city = cityService.createCity("Karlsruhe");
	}

	@Test
	public void testIDsCity() {
		City city2 = cityService.createCity("Munich");
		assertTrue(city2.getId() > city.getId());
	}

	@Test
	public void testGetByID() {
		cityService.createCity("Munich");
		City city2 = cityService.getById(city.getId());
		assertTrue(city.equals(city2));
	}
}
