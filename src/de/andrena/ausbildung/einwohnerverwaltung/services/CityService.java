package de.andrena.ausbildung.einwohnerverwaltung.services;

import java.util.List;

import de.andrena.ausbildung.einwohnerverwaltung.beans.City;

public interface CityService {

	City createCity(String name);

	City getById(long cityId);

	void exportCities();

	void importCities();

	List<City> getAllCities();

}
